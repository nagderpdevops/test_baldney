import os
import sys
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir) 
from src import algo

def test_area():
    output = algo.area_of_rectangle(3,4)
    assert output == 12
    
def test_perimeter():
    output = algo.perimeter_of_rectangle(3,5)
    assert output == 16